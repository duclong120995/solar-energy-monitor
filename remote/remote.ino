#include <LowPower.h>

void setup() {
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
}

void loop() {
  Serial.print(analogRead(A0));
  Serial.print(',');
  Serial.println(analogRead(A1));
  Serial.flush();

  LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
}
