const PORT_NAME = process.argv[process.argv.length - 1];

const SerialPort = require('serialport');
const parser = new SerialPort.parsers.Readline();
const port = SerialPort(PORT_NAME, (error) => {
	if (error) {
		console.error(`Error: ${ error.message }`);
	} else {
		console.log(`Port ${ PORT_NAME } opened.`);
	}
});

port.pipe(parser);
parser.on('data', console.log);
