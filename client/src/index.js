import React from 'react';
import ReactDOM from 'react-dom';
import io from 'socket.io-client';
import { XYPlot, XAxis, YAxis, HorizontalGridLines, LineSeries, AreaSeries } from 'react-vis';
import './index.css';
import '../node_modules/react-vis/dist/style.css';

class Monitor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    }

    setInterval(() => {
      this.updateData()
    }, 1000);
  }

  voltageData() {
    return this.state.data.map((datum, index) => {
      return {
        x: datum.index,
        y: datum.voltage
      }
    });
  }

  currentData() {
    return this.state.data.map((datum, index) => {
      return {
        x: datum.index,
        y: datum.current
      }
    });
  }

  powerData() {
    return this.state.data.map((datum, index) => {
      return {
        x: datum.index,
        y: datum.voltage * datum.current
      }
    });
  }

  updateData () {
    const data = this.state.data.slice();

    let datum = { index: new Date(), voltage: Math.random() * 5, current: Math.random()*100 - 50 };
    data.push(datum);

    if (data.length > 50) data.shift();

    this.setState({
      data: data
    });
  };

  render () {
    var legend = [
      'Voltage',
      'Current'
    ];

    return (
      <div>
        <XYPlot width={ 1000 } height={ 300 }>
          <HorizontalGridLines />
          <LineSeries data={ this.voltageData() } curve={ 'curveMonotoneX' } />
          <XAxis />
          <YAxis />
        </XYPlot>
        <XYPlot width={ 1000 } height={ 300 }>
          <HorizontalGridLines />
          <LineSeries data={ this.currentData() } curve={ 'curveMonotoneX' } />
          <XAxis on0={ true } />
          <YAxis />
        </XYPlot>
        <XYPlot width={ 1000 } height={ 300 }>
          <HorizontalGridLines />
          <LineSeries data={ this.powerData() } curve={ 'curveMonotoneX' } />
          <XAxis on0={ true } />
          <YAxis />
        </XYPlot>
      </div>
    );
  }
}

ReactDOM.render(
  <Monitor />,
  document.getElementById('root')
);
